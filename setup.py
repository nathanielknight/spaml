#!/usr/bin/python
"""
Setup script for the spaml package.  Provides metadata; uses and requires
distutils, the default python package distribution system.
"""

from distutils.core import setup

setup(
        name="Spaml",
        version="0.1",
        description="Succinct Pythonic Advanced Markup Language",
        author="Nathaniel Egan",
        author_email="nathaniel.ep@gmail.com",
        url="https://github.com/neganp/spaml",
        packages=[ "lib/markdown", "utils"],
        scripts=["scripts/spaml"],
        requires=["markdown"]
        )

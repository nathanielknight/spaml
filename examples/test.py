#! usr/bin/python

import os

from spaml import spaml

s = filter(lambda x: x[-5:] == 'spaml', os.listdir('.'))

d = map(lambda x: x.replace('spaml', 'html'), s)

map(lambda x,y: spaml(x,y), s,d)

%html
  %head
    :css
      body {
        width: 70%;
        margin-left: 15%;
      }

      div.example{
        border: 1px solid black;
        overflow:hidden;

      }
      div.example div.source{
        padding: 1em;
        float:left;
        width:45%;
      }
      div.example div.source pre.sourcecode{
        color:blue;
        display: block;
      }

      div.example div.result{
        padding: 1em;
        float:right;
        width:45%;
      }
  :script
    //This isn't useful, but it shows off SPAML's script environment.
    document.write("<h1>SPAML</h1>");

  %body
    %h2 
      The Succinct, Pythonic, Advanced(?) Markup Language.
    
    :markdown
      SPAML is for anyone who likes programming in Python and needs to write
      HTML.  The idea is to make you type less by putting your HTML tags in one
      place and having SPAML figure out where to put the end tags.  
      
      If you've ever used the [Haiku markup language](http://haml-lang.com/) then
      SPAML will will look lawsuit-inducingly familiar.  The significant difference
      is that SPAML replaces Ruby with Python.  
    
    %h2
      The Basics of SPAML Syntax

    %h3
      Creating HTML
    %p
      A SPAML document is a plain-text file that 
      %code spaml.py
      compiles to HTML.  The markup 
      minimizes the number of keystrokes you have to make by using whitespace to show 
      the structure of your document.  
    %div.example
      %div.source
        This source
        %pre.sourcecode
          :plain
            %h2
              Ipsum Lorum
            %p
              Lorem ipsum dolor sit amet, consectetur adipisicing 
              elit, sed do eiusmod tempor incididunt ut labore et 
              dolore magna aliqua.  
              %b
                Ut enim ad minim veniam,
              quis nostrud exercitation ullamco laboris nisi ut 
              aliquip ex ea commodo consequat. 
      %div.result
        Becomes this HTML
        %xmp
          %h2
            Ipsum Lorum
          %p
            Lorem ipsum dolor sit amet, consectetur adipisicing 
            elit, sed do eiusmod tempor incididunt ut labore et 
            dolore magna aliqua.  
            %b
              Ut enim ad minim veniam,
            quis nostrud exercitation ullamco laboris nisi ut 
            aliquip ex ea commodo consequat. 

    %p
      Any HTML tag can be created in this way, and you can nest them as deep as you want (at least up to
      your Python's recursion limit for functions, so if you're into nesting thousands of layers deep you
      may be out of luck).  Additionally, short, one line elements, the content can be put on the same 
      line as the tag, but there can be no indentation block below in that case.
    %div.example
      %div.source
        This source
        %pre.sourcecode
          :plain
            %h1 Features of Kittens
            %ul
              %li Tiny paws
              %li Fluffy tails
              %li High-pitched meows
      %div.result
        Becomes this html.
        %xmp
          %h1 Features of Kittens
          %ul
            %li Tiny paws
            %li Fluffy tails
            %li High-pitched meows


    %h3
      ID and Class Attributes

    %p
      An html tag's attributes can be specified using a Python dictionary.  For example:
    %div.example
      %div.source
        This source
        %code
          %pre.sourcecode
            :plain
              %a{'href':'http://www.wikipedia.org', 'id':'wikilink'}
                Wikipedia
      %div.result
        Becomes this html
        %xmp
          %a{'href':'http://www.wikipedia.org', 'id':'wikilink'}
            Wikipedia


    %h3
      Class and ID Attributes
    %p
      Since 
      %code class 
      and 
      %code id 
      attributes are so common in documents style with CSS, SPAML has a special syntax for specifying them.

    %div.example
      %div.source
        %pre.sourcecode
          :plain
            %p.quote#zappa
              Information is not knowledge. 
              Knowledge is not wisdom. 
              Wisdom is not truth.  
              Truth is not beauty. 
              Beauty is not love. 
              Love is not music. 
              Music is the best!
            %p.quote#picard
              Make it so.
      %div.result
        %xmp
          %p.quote#zappa
            Information is not knowledge. 
            Knowledge is not wisdom. 
            Wisdom is not truth.  
            Truth is not beauty. 
            Beauty is not love. 
            Love is not music. 
            Music is the best!
          %p.quote#picard
            Make it so.
    %p
      You can add as many classes and ids as you want to a tag in this way and it combines safely with 
      attributes specified in a dictionary.  

    %h3
      Other Tags
    %p
      In addition to HTML tags, SPAML has a few other features.
    %ul
      %li
        Starting a line with a 
        %code \ 
        escapes a first character that would otherwise be interpreted as a tag.
      %li
        Starting a line with an
        %code
          \=
        marks it as a line of python code, which SPAML will execute.  Anything sent to the standard
        output in this kind of line is inserted into the document.  The document has its own namespace,
        so variables defined in one line will persist into other lines.

    %div.example
      %div.source
        %pre.sourcecode
          :plain
            If you try to write a 
            % at the beginning of a line it doesn't work so well.

            But if you escape it like this:
            \% it works just fine.

            If you have a lot of numbers to write, 
            just do it like this:
            =for i in range(20): print i,
      %div.result
        %xmp
          If you try to write a 
          % at the beginning of a line it doesn't work so well.

          But if you escape it like this:
          \% it works just fine.

          If you have a lot of numbers to write, 
          just do it like this:
          =print ' '.join([str(i) for i in range(20)])




    %h2
      Environments
    %p
      In addition to HTML, SPAML has several special environments specified witha ":".

    %dl
      %dt 
        The Python environment, 
        %code :python
      %dd
        The big brother to the 
        %code =
        tag, this block will be treated as python code, executed, and anythin sent to the standard output 
        will be inserted into the document.  

      %dt
        The Plain environment,
        %code :plain
      %dd
        A plain environment is interpreted as plain text&mdash;it is passed to the document as is.  It doesn't
        add a 
        %code pre
        tag to force the browser to preserve formatting, so if that is the desired result, the 
        %code :plain
        block should be nested within a 
        %code pre

      %dt 
        The CSS environment, 
        %code :css
      %dd
        This environment passes its contents through unchanged except for surrounding it by
        style tags with 
        %code type="text/css"

      %dt
        The Script environment, 
        %code :script
      %dd
        Like the CSS environment, the contents are passed through surrounded by script tags with 
        %code type="text/javascript" 
        but otherwise unchanged.

      %div.example
        %div.source
          %pre.sourcecode
            :plain
              :python
                x = "-'
                for i in range(7): print i * x

              :plain
                %p
                  Normally i'd be a paragraph, but here I'm just words.
                %p
                  And if you don't tell the browser to honour 
                  my line breaks, I look like this!

              :script
                document.write("I'm from a script!");

              :css
                p.asdf{
                  color: green;
                }
              %p.asdf
                I'm envious.
        %div.result
          :python
            x = "-"
            for i in range(7): print i * x

          %pre
            :plain
              %p
                Normally i'd be a paragraph, but here I'm just words.
          :plain
            %p
              And if you don't tell the browser to honour my line breaks, I look like this!

          :script
            document.write("I'm from a script!");
          :css
            p.asdf{
              color: green;
            }
          %p.asdf
            I'm envious.




#! /usr/bin/python
"""
Executable element of the spaml package.
"""

import sys
import spaml

def f(text_lines):
    """
    Convert text lines from spaml source to html. 

    Helper function that applies the correct combination of spaml util
    functions and native conversions to go from source to output.  Output
    is returned as a list.
    """
    return [str(i) for i in spaml.utils.parse(text_lines)]


#Set defaults
instream, outstream = sys.stdin, sys.stdout
#Check if defaults are over-ridden
if len(sys.argv) > 1:
    instream = file(sys.argv[1], 'r')
if len(sys.argv) > 2:
    outstream = file(sys.argv[2], 'w')

#Clean input of trailing \n's (upsets the dicts in utils/handlers otherwise)
source = [i.rstrip('\n') for i in instream.readlines()]

#Convert and output
outstream.writelines(
        f(source))

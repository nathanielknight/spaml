#! /usr/bin/python
'''Welcome to spaml.py, the

    Succinct
    Pythonic
    Advanced (?)
    Markup
    Language

Spaml is a clone of Haml, but for Python rather than Ruby.  See the readme
for more information on what Spaml can do and how it can be used.


Spaml can be invoked several ways.  

As a standalone program, spaml reads from the standard input and writes to
the standard output.  It can also take filenames, with the syntax

    spaml infile outfile

As a module imported into other programs, the functionality can be accessed
by means of the spaml.spaml function.  It can be called in a variety of ways,
and is documented below.

By: Nathaniel Egan-Pimblett
Date: January 12th 2011
'''

import sys
import utils


#Defaults
INDENT = 2

def spaml(*args):
    """
    Transform spaml source into html output. 
    
    The source code read from a filestream or list is transformed to html
    output, which are either written to a file object or returned as a list.

    The first argument is either a filename from the current working
    directory, a file object open for reading, or a list of strings to be interpreted as
    the source.

    The second argument (if present) is a filename in the current working
    directory or an open file 

    """
    #Figure out what the arguments mean
    if len(args) == 0:
        raise TypeError("spaml requires at least one argument")
    #Figure out the input
    instream = args[0]
    if type(instream) not in [str, file, list]:
        raise TypeError("spaml only handles filenames, input files, or lists of input")
    if type(instream) == str:
        instream = open(instream, 'r')
    if type(instream) == file:
        if instream.mode != 'r': 
            raise IOError("Input file must be open for reading.")
        instream = instream.readlines()
    for i in instream:
        i.rstrip('\n') #trailing newlines can confuse the handlers in utils/
    #Figureout the output
    if len(args) > 1:
        outstream = args[1]
        if type(outstream) not in [str, file]:
            raise TypeError("spaml can only write to files")
        if type(outstream) == str:
            outstream = open(outstream, 'w')
        if outstream.mode != 'w':
            raise TypeError("Output file must be open for writing.")
        
    #Turn the source into html
    x = utils.parse(instream)
    #Force each element to render to HTML/ECMAScript/CSS etc.
    x = map(lambda x: str(x), x)

    #Write the html to file or return it as a list.
    if len(args) == 1:
        return x
    else:
        outstream.writelines(x)
        return None


#Execution as a standalone module.
if __name__ == "__main__":
    """
    If executed as a module, spaml takes input from one file and prints it to
    another.  It can be invoked in three ways.
    
    cat inputfile.spaml | python spaml > outputfile.html

    python spaml inputfile.spaml > outputfile.html

    python spaml inputfile.spaml outputfile.html
    """
    instream, outstream = sys.stdin, sys.stdout
    if len(sys.argv) > 1:
        instream = file(sys.argv[1], 'r')
    if len(sys.argv) > 2:
        outstream = file(sys.argv[2], 'w')

    outstream.writelines(
            spaml(
                instream.readlines()))

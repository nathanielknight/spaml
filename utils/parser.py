'''
Utilities and heavy lifting for spaml.py
'''

#constants
INDENT = 2
SPAML_FLAGS = ['%', '\\', ':', '-', '=', '~']

import sys
from handle import tag_handler

def vunpack(t,n,defaults=None):#{{{
    '''
    Variable length tuple unpacker as seen at  http://code.activestate.com/recipes/392768-padding-variable-length-sequences/
    Original by Greg Jorgensen.

    Return a tuple consistingof the first n items of iterable t.
    Missing elements are filled in with default (or None if default isn't
    supplied).
    '''
    result = list(defaults) + [None] * n
    result[0:len(t)] = list(t)
    return tuple(result[0:n])#}}}


class SpamlBlock:#{{{
    '''
    A block of lines in the document.
    '''
    def __init__(self, head=None, contents=[]):
        self.head = head
        self.contents = contents

    def __str__(self):
        """

        The return value of this function is the value of any SpamlBlock
        instance converted to a string.  Calls self.render, which produces a
        list of lines of text of output.  These are concatennated, separated
        by literal spaces to preserve some formatting between source and
        output.
        """
        return reduce(lambda x,y: x+'\n'+y, self.render(), '')

    def render(self):
        """
        Output the desired html/css/ECMAscript etc.

        Relies on handler functions (in handlers.py) to convert each kind of
        block to the approrpriate html (including classes, ids, etc.  Names
        of tags (as deduced from the head variable) must be exact or it
        raises a KeyError as it tries to find the appropriate handler
        function.
        """
        #Before and after are the HTML tags.  We require at least a before
        #tag, the default for self-closing tags.
        before, after, contents = vunpack(tag_handler(self.head,
            self.contents), 3, defaults=[])
        #If a tag produces no contents (nothing to go between the tags)
        #vunpack will assign a None-value to the contents variable above.
        #In this case, we want to print None between the two tags.
        if contents != None:
            self.contents = contents

        #Return the rendered output as a list of strings.
        output = []
        output.extend(before)
        for i in self.contents:
            if type(i) == str:
                output.append(i)
            else:
                output.extend(i.render())
        output.extend(after)
        return output#}}}


class SpamlError(Exception):#{{{

    def __init__(self, line_number=0, line='', message=''):
        sys.stderr.write(
                "Syntax Error  line {0}:{1}\n  {2}\n\n".format(
                    line_number, message, line))
        return None
#}}}
        

def deindent(lines, ind=INDENT):#{{{
    """
    Deindent lines of spaml source.

    Remove ind spaces from the beginning of each line, ignoring blank or
    empty lines.
    """
    result = []
    for i in lines:
        if len(i) == 0 or i.isspace():
            #Ignore (pass through) empty lines.
            result.append(i)
        else:
            result.append(i[2:])
    return result#}}}


def leading_spaces(string):#{{{
    return len(string) - len(string.lstrip())#}}}


def first_chunk_of(l):#{{{
    #A chunk of one line must be a one-line chunk.
    if len(l) == 1:
        return l
    #A chunk not followed by an indentated block must be a one-line chunk.
    if leading_spaces(l[1]) == 0:
        return [l[0]]
    #Otherwise, the chunk consists of all lines until the indented block is
    #broken.
    i = 1 #starting at 2 incorectly chunks single indented lines
    while leading_spaces(l[i]) >= leading_spaces(l[1]) or l[i] == '' or l[i].isspace():
        i += 1
        if i == len(l):#stop if we reach the end of the input chunk
            break
    return l[:i]
#}}}


"""
Execution is passed back and forth between parse and subparse until all of
the input has been considered.  Parse divides the input into appropriate
chunks based on indentation.  Subparse decides what to do with the chunks.
"""

def subparse(n):#{{{
    """
    Given a block of indented source, examine it do decide what kind of
    processing it requires to become SpamlObjects.
    """
    head = n[0]
    contents = n[1:]
    if len(contents) == 0:
        contents = None
    #Don't touch plain-text (allows for raw HTML)
    if head[0] not in SPAML_FLAGS:
        return n
    #Don't sub-chunk special flags (e.g. :python and :css)
    if head[0] == ':':
        return [SpamlBlock(head=head, contents=contents)]
    #Do sub-chunk regular flags
    else:
        #certain tags preclude contents; the parser includes newlines if
        #they are followed by empty lines; ignore forcefully
        if contents == None or head[0] == '\\' or head[0] == '=':
            return [SpamlBlock(head=head)]
        else:
            return [SpamlBlock(head=head, contents=parse(deindent(contents)))] #}}}

    

def parse(tl, ind=INDENT):#{{{
    """
    Pass through whitespace and plain text, make anything else a spaml-block.

    Divides lines of input (as a list of strings) into apprpriately grouped
    chunks to be interpreted as spaml-source.  Plain and empty lines are
    simply passed through as strings.  Blocks with content (SpamlBlock
    candidates themselves) are parsed into SpamlBlocks which can be rendered
    to strings using the functions in handlers.py
    """
    chunks = []
    i = 0

    while len(tl) > i:
        n = first_chunk_of(tl[i:])
        if len(n) == len(tl[i:]):
            chunks.extend(subparse(n))
            return chunks
        else:
            chunks.extend(subparse(n))
            i += len(n)

    #parsing has finished, return the contents
    return chunks#}}}

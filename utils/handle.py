'''
Functions to handle different types of input, changing it from 
SPAML to html, css, javascript, Django tags, etc.
'''

import sys
#third party libraries
try:
    import markdown
    mdConverter = markdown.Markdown()
except ImportError:
    sys.stderr.write(
    "python-markdown package not found--no support for ':markdown' tag.\
    Acquire python-markdown at http://pypi.python.org/pypi/Markdown")


#------------------------------------------------------------------
# Constants and Tools

# An incomplete list of legal html tags to validate incoming spaml.
#HTML_ELEMENTS = [ "html", "head", "body", "blockquote", "div", "dl", "dd", "dt", "h1", "h2", "h3", "h4", "h5", "h6", "hr", "ol", "ul", "li", "p", "pre", "a", "b", "em", "i", "br", "cite", "code", "dfn", "img", "span", "sub", "sup", "tt", "style", "script", ]

SPAML_FLAGS = ['%', '\\', ':',  '=' ]
INDENT = 2

# Namespace for embedded python code to be carried out in.
DOC_NAMESPACE = {}


class StringBuffer(list):
    '''
    This object catches strings written to it as a file and regurgitates them
    when render(self) is called, simultaneously wiping its contents clean.

    It is used in the implementation of the :python block and th '='
    one-line-python tag so that anything written to sys.stdout in those
    blocks is redirected to the output in the right order.
    '''
    
    def write(self, word):
        """Appends a string to the buffer's contents."""
        assert type(word) == str
        if word != '\n':
            self.append(word)

    def writelines(self, lines):
        """Appends a list of strings to the buffer."""
        assert type(lines) == list
        for i in lines:
            assert type(i) == str
        self.extend(
                filter(lambda x: x!='\n', lines))
    
    def render(self):
        """Wipe the contents of the buffer and return its contents as a list
        of strings."""
        x = self[:]
        del self[:]
        return x


class CaughtOutput():
    """
    Return a context guard for executing python code in SPAML documents.

    When used as a with CaughtOutput() as sys.stdout: statment, replaces the
    standard output with a StringBuffer instance which catches anything sent
    to the standard output and inserts it into the document.  sys.stdout is
    then restored to normal.
    """

    def __enter__(self):
        """Retain the existing sys.stdout when entering the context."""
        self.old_stdout = sys.stdout
        return StringBuffer()

    def __exit__(self, type, value, traceback):
        """Restore the original sys.stdout when leaving the contenxt."""
        sys.stdout = self.old_stdout


def first_word(x):
    """
    Return the contents of a string until one of the following is
    encountered: [' ', '.', '#', '{'].

    This function is used in parsing the heads of SpamlBlocks, finding the
    tag in an element block, or the name of a div/id specified with the
    '$element.div#id' syntax.
    """
    if x == None:
        return None
    # Process the head
    i=0
    while True:
        if i == len(x)-1:
            return x
        elif x[i] in " .#{": 
            return x[:i]
        i+=1


def attributes_string(attrs):
    '''
    Given a list of key-value pairs, return a string of the form
    "key='value'" joined by spaces.

    This function is used when parsing html blocks from SpamlBlock
    representation to HTML, particularly in rendering the attributes of an
    HTML element.
    '''
    classes = filter(lambda x: x[0]=='class', attrs)
    ids = filter(lambda x: x[0]=='id', attrs)
    others = filter(lambda x: x[0]!='class' and x[0]!='id', attrs)
    #
    if len(classes) == 0:
        cstring = ''
    else:
        cstring = "class='{0}'".format(' '.join(map(lambda x: x[1], classes)))
    if len(ids) == 0:
        istring = ''
    else:
        istring = "id='{0}'".format(' '.join(map( lambda x: x[1], ids)))
    if len(others)==0:
        ostring = ''
    else: 
        ostring = ' '.join( map(
            lambda x,y: "{0}='{1}'".format(x,y),
            [x[0] for x in others], [x[1] for x in others]))
    output =' '.join([istring, cstring,ostring]).strip() 
    if len(output) == 0: return ''
    else: return ' ' + output



#------------------------------------------------------------------
# Handlers
def elementHandler(head, contents):
    """
    Return the before tag, after tag, and contents of an HTML element.

    Given the head and contents of a SpamlBlock corresponding to an HTML
    element, this function parses the head to find the appropriate opening
    and closing tags, as well as the element's attributes, and returns them
    as three lists of strings in the order [before], [after].  It does not
    modify the contents.
    """
    x = head.lstrip("%").lstrip()
    attrs = []
    inner_text = ''
    tag = first_word(x)
    x = x.lstrip(tag)
    while True:
        if len(x) == 0:
            break
        if x[0] == ' ':
            x = x.lstrip()
            continue
        # id
        if x[0] == '#':
            x = x[1:]
            attrs.append( ('id',first_word(x)) )
            x = x.lstrip(first_word(x))
            continue
        # class
        if x[0] == '.':
            x = x[1:]
            attrs.append( ('class', first_word(x)) )
            x = x.lstrip(first_word(x))
            continue
        # attrs in a python dict
        if x[0] == '{':
            if x.index('}') == len(x):
                d = x
                x = ''
            else:
                d = x[:x.index('}')+1]
            exec 'd = {0}'.format(d)
            for i in d.keys():
                attrs.append( (i, d[i]))
            x = x[x.index('}')+1:]
            continue
        # Assume the remainder is text to go inside the tag.
        inner_text = x
        x = ''
    before = "<{0}{1}>{2}".format(tag, attributes_string(attrs), inner_text)
    after = "</{0}>".format(tag)
    return [before], [after]


def escapeFirstHandler(head, contents):
    """Ignores the first character of a line when it would otherwise be
    interpreted as a SPAML tag."""
    if contents != []:
        raise Exception("Block error: escapeFirst blocks can't have content")
    else:
        return [head[1:]], contents


def pythonHandler(head, contents):
    """Execute one line of python code, inserting anything written sys.stdout
    into the document."""
    assert contents == []
    with CaughtOutput() as sys.stdout:
        exec head[1:] in DOC_NAMESPACE
        return [], [], sys.stdout.render()

def djangoHandler(head, contents):
    """Interpret this line as the contents of either a Django variable
    or a Django tag, and return a [before] containing the appropriate
    text."""
    if contents != []:
        raise Exception("Django tags shouldn't have content.")
    if head[1] == '%':
        surround = '{%', ' %}'
        text = head[2:]
    else:
        surround = '{{', ' }}'
        text = head[1:]
    # Using _string concatenation because {, }, % collide with 
    # Python's format specification mini-language.
    return [surround[0] + text + surround[1]], [], []


def markdown_handler(head, contents):
    """Convert the text in this block from Markdown to HTML using the
    python-markdon package."""
    if contents == []:
        return [], [], []
    else:
        mdString = "\n".join(contents)
        htmlString = str(mdConverter.convert(mdString))
        htmlList = htmlString.split('\n')
        return [], [], htmlList

    


#------------------------------------------------------------------
# Special blocks
def plainHandler(head, contents):
    """
    Ignore SPAML tags within this block, passing through its contents
    unmodified.

    This function is used to write plain text that would otherwise be mangled
    as SPAML tried to interpret it as SPAML commands.  It does not, however,
    surround its contents with <pre> tags,so the result may not be rendered
    as it appears in the source.
    """
    if contents == []:
        return [], []
    else:
        # Append line-breaks to the contents so that breaks in the
        # source are rendered
        return [], [], contents #map(lambda x: x+'<br/>', contents)


def css_handler(head, contents):
    """
    Do nothing to the contents of the block, but surround it by style tags
    with the text/css type.
    """
    if contents == []:
        return [],[]
    else:
        before = "<style type='text/css'>"
        after = "</style>"
        return [before], [after]


def ECMAscriptHandler(head, contents):
    """
    Do nothing to the contents of the block, but surround it by script tags
    with the text/javascript type.
    """
    if contents == []:
        return [], []
    else:
        before = "<script type='text/javascript'>"
        after = "</script>"
        return [before], [after]


def pythonBlockHandler(head, contents):
    """
    Execute the (properly formatted) python code in this block, inserting
    anything text sent to sys.stdout into the document.

    This block treats the column INDENT (default=2) spaces to the right of
    its header as the left margin for the purposes of formatting python code,
    and anything written before that will be ignored.
    """
    #sys.stderr.write(r'\n'.join([i[INDENT:] for i in contents]))
    with CaughtOutput() as sys.stdout:
        exec '\n'.join([i[INDENT:] for i in contents]) in DOC_NAMESPACE 
        return [], [], sys.stdout.render()


def escapeHTMLHandler(head, contents):
    """
    Replace < and > with character codes for safe rendering of Hyptertext
    Markup in HTML.
    """
    f = lambda x: x.replace('<', '&lt;').replace('>', '&gt;')
    return [], [], map(f, contents)
    






#------------------------------------------------------------------
# Externals
special_tags = {
        'css': css_handler,
        'plain': plainHandler,
        'script': ECMAscriptHandler,
        'python': pythonBlockHandler,
        'escapeHTML': escapeHTMLHandler,
        'markdown': markdown_handler,
}

def special_handler(head, contents):
    """Return the output of the appropriate handler given a SpamlBlock's head
    and contents."""
    return special_tags[head[1:]](head, contents)


handlers = {
        '%': elementHandler,
        '\\': escapeFirstHandler,
        '=': pythonHandler,
        ':': special_handler,
        '~': djangoHandler,
}

def tag_handler(head, contents):
    """
    Return the appropriate before tag, after tag, and contents for a
    SpamlBlock as lists and in that order.

    Given the head and contents of a SpamlBlock, this function attempts to
    parse it into appropriate HTML, returning its output as three lists:
    before, after, contents

    before is a list of strigns which are to be placed before the contents
    (usually opening HTML tags) while after is a list of strings to be placed
    after (usually closing HTML tags).
    """
    
    if head == None:
        return [], []
    assert type(head) == str
    head = head.rstrip('\n')
    return handlers[head[0]](head, contents)
